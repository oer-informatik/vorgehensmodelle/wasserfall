## Royce' Kritik am einfachen Phasenmodell (Wasserfallmodell)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110454170489315291</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/royce-kritik-am-wasserfall</span>

[comment]: # (Den oberen und den unteren Teil kopieren)


> **tl/dr;** _(Bearbeitungszeit ca. 90 min): Die meisten Auszubildenden nutzen in ihren Abschlussarbeiten das "Wasserfallmodell" basierend auf einer Grafik, die Winston W. Royce als Beschreibung einer unbefriedigenden Ausgangslage herangezogen hat. Dass er daraufhin fünf wesentliche Schritte formuliert hat, die diesem Modell hinzugefügt werden müssen, um Risiken zu minimieren, ist über die Zeit scheinbar wieder verloren gegangen. Wir versuchen uns diesen fünf Schritten über das Originaldokument und Leitfragen zu nähern._

### Kontext und Einordnung

Dass ausgerechnet Winston W. Royce oft als "Erfinder des Wasserfallmodells" bezeichnet wird, ist auf viele Arten ungewöhnlich: Weder hat er den Begriff "Wasserfall" dem Ursprungsartikel verwendet, noch ist er in diesem Artikel ein Verfechter dieses Modells. Vielmehr dient die oft zitierte Grafik, die dann Betitelungen wie "Wasserfallmodell mit Rücksprung" bekommt, in seinem Artikel lediglich dazu, zu zeigen, an welchen Stellen dieses verbreitete Vorgehen verbessert werden muss. 

![Royce hat diese Grafik nur erstellt, um auf deren Probleme hinzuweisen. Das dann "Wasserfallmodell nach Royce" zu nennen, ist etwas irreführend. (angelehnt an _Figure 3_ seines Artikels)](images/wasserfall-einfach.png)

Royce benennt viele Probleme beim einfachen phasenorientierten dokumentengetriebenen Vorgehen, die auch viele iterativ-inkrementelle Vorgehensmodelle adressieren: in dem Artikel aus 1970 kann man daher auch einen frühen Vorboten der Agilität mit den Mitteln seiner Zeit sehen.^[z.B. [Jens Himmelreich: "Agile Software Entwicklung nach Winston Royce"](https://jenshimmelreich.de/_texte/AgileSoftwareentwicklungNachWinstonRoyce.pdf)]

Royce hat den Artikel [Managing the development of large software systems](https://dl.acm.org/doi/pdf/10.5555/41765.41801) zu einer Zeit geschrieben, als häufig die begrenzten Ressourcen eines Microcontrollers für Scheitern von Projekten verantwortlich waren: Speicherplatz (Storage), Laufzeit (Timing) und Datenfluss (Dataflux). Da diese Limitierung heute bei vielen Projekten nicht mehr zutrifft, müssen seine Aussagen im damaligen Kontext eingeordnet und auf die heutigen limitierenden Faktoren übertragen werden!

Wir wollen uns diesem Artikel über Leitfragen nähern:

Lest bitte abschnittweise den Artikel von Winston W. Royce [Managing the development of large software systems](https://dl.acm.org/doi/pdf/10.5555/41765.41801) und beantwortet für jeden Abschnitt die folgenden Leitfragen. Ich empfehle DeepL & Co beiseite zu lassen, und den Text direkt auf Englisch zu lesen. Damit das allen einfacher fällt habe ich ein paar Schlüsselbegriffe aus den Seiten notiert.

### Einführung: Computer Programm Development Functions:

  |eng|dt|
  |:---|:---|
  |salvageable|verwertbar|
  |preserved|gut erhalten|
  Table: Englische Begriffe aus S. 328

- Welche zwei Schritte sieht Royce als essenziell in allen Softwareentwicklungsprozessen an?

<span class="delete-answer" style="display:none">

  - Analysis
  - Coding

</span>

- Unter welchen Voraussetzungen reicht die Gliederung in diese beiden Schritte seiner Meinung nach aus?

<span class="delete-answer" style="display:none">

  - small effort
  - final product is to be operated by those who built it.

</span>

- Warum werden die zusätzlichen Schritte (v.a. vom Kunden) häufig als nicht nötig angesehen?

<span class="delete-answer" style="display:none">

  - kein direkt ersichtlicher Beitrag zum Projektergebnis
  - Erhöhung der Kosten

</span>
  
  |eng|dt|
  |:---|:---|
  |invariable|ausnahmslos|
  |octal patch| (vermutlich meint er händische Anpassung im compilierten ( Assembler-)-Code)|
  |disruptive| (zer-)störend|
  Table: Englische Begriffe aus S. 329

- Welche Probleme sieht Royce in seinem zweiten Modell nach der Testphase?

<span class="delete-answer" style="display:none">

  - Probleme tauchen erst in der Testphase auf und führen dazu, dass die Anforderungen grundlegend geändert werden müssen.

</span>


### Step 1: "Program Design comes first"

  |eng|dt|
  |:---|:---|
  |preliminary|vorbereitend|
  |to culminate in sth.|in etwas gipfeln|
  |allocation|Zuweisung|
  |insufficient|ungenügend|
  Table: Englische Begriffe aus S. 331

- Welche Kritikpunkte sieht Royce am Konzept "Program Design comes first"?

<span class="delete-answer" style="display:none">

  - Da dem Programmdesigner noch keine Analyse-Ergebnisse vorliegen muss er im "Vakuum" planen und wird Fehler machen.

</span>

- Welche Verbesserungen verspricht er sich von "Program Design comes first"?

<span class="delete-answer" style="display:none">

  - Eine Designphase zwischen "Software Requirements" und "Analysis" sorgt dafür, dass bereits in der Analysephase das Programmdesign für Speicherung, Timing und Datenfluss (den damaligen Flaschenhälsen) vorliegen und den Analytikern bewusst sind.

</span>

- Die genannten Engpässe (Timing, Storage, Dataflux) spielen heutzutage oft keine große Rolle mehr. Welche Schlüsse können wir trotzdem heute aus dem Absatz "Program Design comes first" ziehen?

<span class="delete-answer" style="display:none">

- Flaschenhalse früh erkennen und in die Planungen einbeziehen

</span>


### Step 2: "Document the Design"

  |eng|dt|
  |:---|:---|
  |ruthless|schonungslos|
  |intangible|nicht greifbar|
  |unequivocal|unmissverständlich|
  |downstream|Nachgelagert (eigentlich: Ausfluss / talseitig)|
  Table: Englische Begriffe aus S. 332

- Aus welchem Grund spricht sich Royce für stringente Dokumentation aus? In welchen Phasen sieht er welche Vorteile?
 
 <span class="delete-answer" style="display:none">
  - Verbindliche und belastbare Kommunikation
  - In den frühen Phasen (Analyse, Design) existiert keine anderen Artefakt: die Dokumentation ist die Spezifikation und das Design.
  - In den nachgelagerten Phasen beginnt der eigentliche Vorteil:
    - in der Testphase kann dank guter Dokumentation jeder das Programm testen, nicht nur der jeweilige Entwickler, der die Fehler selbst gemacht hat
    - im Betrieb kann eine gut dokumentierte Software von dritten betrieben und gewartet werden - ohne Dokumenation nur von denjenigen, die das Projekt programmiert hatten. Gute Dokumentation kann verhindern, dass im Fehlerfall die Software zu unrecht als Verursacher verdächtigt wird - oder schnell das fehlerhafte Modul gefunden werden kann.
  - Bei Weiterentwicklung kann auf die bestehende dokumentierte Software aufgebaut werden. Ohne Dokumentation muss oft von vorne angefangen werden.

</span>

### Step 3: "Do it twice"

  |eng|dt|
  |:---|:---|
  |leverage|Einfluss|
  |to arrange matters|die Belange anordnen/sortieren|
  |to utilize|benutzen|
  |pilot effort|erste Versuch|
  |straightforward|direkt / einfach|
  Table: Englische Begriffe aus S. 334

- Welche Kompetenzen benötigen die Entwickler*innen, die am Pilotversuch beteiligt sind?

<span class="delete-answer" style="display:none">

        Intuitives Gefühl für Analyse, Design und Coding
        Gabe, Fehler im Design zu erkennen und Alternativen schnell modellieren zu können
        die unkomplizierten Anforderungen herauskristalisieren können, die nicht gesondert im Pilotversuch eingebaut werden müssen
</span>

- Welche Vorteile bietet ein vorgeschalteter Pilotversuch?

<span class="delete-answer" style="display:none">

        Kritische Abläufe können bereits getestet/simuliert werden und Risiken können besser eingeschätzt werden.

</span>


### Step 4: "Plan, control and monitor Testing"

  |eng|dt|
  |:---|:---|
  |feasible|machbar|
  |proofreading|Korrekturlesen|
  |to plow|pflügen|
  |to obscure sth|etwas verschleiern|
  |to give free rein to sth.|etwas / jemandem freien Lauf lassen|
  |to bolster|unterstützen|
  Table: Englilsche Begriffe aus S. 335

- Welche Kernpunkte spricht Royce für die Testphase an?

<span class="delete-answer" style="display:none">

        Testspezialisten sind die besseren Tester - wenn sie etwas nicht testen "können" - sondern nur der Entwickler - dann ist das Projekt unzureichend dokumentiert.
        Es sollte ein Review aller Komponenten geben - Logische Fehler kann man nur bei Korrekturlesen finden.
        Jeder Pfad eines Softwaresystems sollte einmal durchlaufen werden.
        Ein abschließender Funktionstest muss erfolgen

</span>

### Step 5: "Involve the customer"

- Wann sollte der Kunde eingebunden werden?

<span class="delete-answer" style="display:none">

  - "at earlier points" - an formal festgelegten frühen Zeitpunkten mit drei Review-Terminen

</span>


### Fazit: Ist Royce der erste agile Softwarentwickler?

Am Ende hat Royce aus dem ursprünglichen zwei Phasen-Modell einen relativ komplexen Ablauf entwickelt. Die spannendste Frage ist: wie hätte sein Dokument ausgesehen, wenn er es dreißig Jahre später geschrieben hätte?

![Das abschließende Modell von Royce mit den fünf zusätzlichen Schritten" "Programm design comes first", "Document the design", "Do it twice", "Plan, control and monitor tests", "Involve the customer" (angelehnt an _Figure 10_ seines Artikels)](images/wasserfall-am_ende.png)

- In welchen Bereichen verfolgt Royce die gleichen Ziele wie agile Vorgehensmodelle?

- In welchen Bereichen weicht er von agilen Prinzipien ab?

Royce ist angetreten, um Risiken zu minimieren, das Scheitern von Projekten zu erschweren. Er ist nicht "Vater des Wasserfallmodells", sondern einer der ersten, der dieses Vorgehen hinterfragt hat. Wir sollten ihn nicht mit der Ausgangsgrafik seiner Problemanalyse zitieren, als wäre es seine Idee, so vorzugehen.  

### Links und weitere Informationen

- Zentrales Dokument ist natürlich der Artikel von Winston W. Royce von 1970,[Managing the development of large software systems](https://dl.acm.org/doi/pdf/10.5555/41765.41801)

- Einen sehr lesenswerten Versuch, Royce Artikel in eine Reihe mit agilen Vorgehensmodellen einzuordnen hat Jens Himmelreich in seinem Artikel ["Agile Software Entwicklung nach Winston Royce"](https://jenshimmelreich.de/_texte/AgileSoftwareentwicklungNachWinstonRoyce.pdf) gemacht, Leseempfehlung! (PDF aus: Bernd Oestereich (Hrsg.): Agiles Projektmanagement, Beiträge zur interdisziplinären Konferenz interPM, 2006, dpunkt-Verlag, ISBN-13: 978-3898644129)

