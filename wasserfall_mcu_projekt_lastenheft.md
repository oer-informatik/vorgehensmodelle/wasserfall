## Lastenheft für ein Microcontroller-Projekt

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/wasserfall_mcu_projekt_lastenheft</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111016716139107981</span>

> **tl/dr;** _(ca. 6 min Lesezeit): Wird ein Microcontroller-Projekt nach dem Wasserfall-Modell entwickelt, dann müssen zunächst die Anforderungen aus Auftraggebersicht definiert und in einem Lastenheft dokumentiert werden. Welche Aspekte sollten hierbei erfasst werden?_

Das Wasserfallmodell teilt den Entwicklungsprozess in unterschiedliche Phasen auf, die jeweils durch die Erstellung eines Artefakts (Dokuments) abgeschlossen werden. Häufig werden die Phasen genutz, wie sie Winston Royce in seinem Artikel ["Managing the development of large software systems"](https://oer-informatik.de/royce-kritik-am-wasserfall) beschrieben hat. Der Ablauf könnte etwa so aussehen:

![Die Phasen des Wasserfall-Modells](images/wasserfall.png)

Die erste Phase stellt die Anforderungsdefinition durch den Kunden dar:

### Anforderungsdefinition durch den Kunden (_customer requirements_,"Was und wofür?", Lastenheft) 

#### Kunde: Was brauche ich und was habe ich davon schon?

Bei Kundenprojekten liegt zunächst der Ball im Feld der Auftraggebenen: sie müssen die grundlegenden Anforderungen an ein Produkt beschreiben: Hier sollte die Frage nach dem _was und wofür_ beantwortet werden. 

Häufig muss dafür der bestehende Prozess, der optimiert werden soll, betrachtet werden ("Ist-Analyse"), um die gewünschten Verbesserungen deutlicher vor Augen zu haben. Falls kein bestehender Prozess existiert, kann es helfen, ähnliche Vorgänge oder Beobachtungen bei anderen gezielt zusammenzutragen.

Diese Ist-Analyse wird um die Verbesserungen und Anpassungen ergänzt, um so den Zielzustand zu formulieren ("Soll-Analyse"). Aus diesem Zielzustand müssen nun Stück für Stück Anforderungen festgelegt und formuliert werden.

Diese Formulierungen münden in einem Lastenheft. In diesem werden zwei unterschiedliche Arten von Anforderungen beschrieben:

- Funktionale Anforderungen: Welches Ergebnis oder welchen Nutzen erbringt das System? 

- Qualitätsanforderungen: Welchen weiteren Anforderungen muss das System genügen, während es den Nutzen erbringt? Gängige Kategorien sind: Bedienbarkeit, Zuverlässigkeit, Effizienz, Wartbarkeit, Nachhaltigkeit.

Technische Details und Realisierungswege werden hier also noch nicht genannt: es geht nicht um Wege, sondern um Ziele.

In der Praxis ist dem Auftraggeber zu diesem Zeitpunkt oft noch gar nicht klar, was er genau will. Sofern bereits Kontakt zu (potenziellen) Auftragnehmenden besteht, ist daher wichtig, bereits in dieser Phase möglichst viele Unklarheiten zu beseitigen. Es gibt eine Reihe von Techniken der [Anforderungs-Ermittlung](https://oer-informatik.de/anforderungsanalyse-ermittlung), die hier angewandt werden können: Workshops, gezielte Befragungen (Inteview oder Frageboden), Prototypen, Marktbeobachtung.

Im Lastenheft sollte beispielsweise festgehalten werden, welche Werte ein IoT-Gerät messen und anzeigen soll, über welche Entfernungen es kommunizieren soll, wie lange die Akkulaufzeit mindestens betragen muss (wenn es sich um ein tragbares Device handelt). Hierbei steht die technische Realisierung wie die Auswahl der Komponenten und Protokolle im Hintergrund: es sollte allein der gewünschte Nutzen beschrieben werden, ohne sich bereits auf einzelne Lösungen festzulegen.

Nach Möglichkeit sollten die Anforderungen bereits kategorisiert und priorisiert werden (siehe z.B. [Anforderungs-Validierung und Verwaltung, MoSCoW-Methode](https://oer-informatik.de/anforderungsanalyse-validieren-verwalten#gliederung-nach-realisierungspriorit%C3%A4t)), um optionale von zwingenden Anforderungen zu trennen.

#### Beispiel: einfaches tabellarisches Lastenheft für kleine Projekte

Der Aufwand für die Erstellung eines Lastenhefts sollte im Verhältnis zum Projektumfang stehen. Für sehr kurze Projekte reicht gegebenenfalls bereits eine kurze Stichpunktliste. Für Projekte, die sich über mehrere Wochen / Monate hinziehen sollte systematisch ein Lastenheft erstellt werden, um keine wesentlichen Punkte zu vergessen.

Ein einfaches Lastenheft kann tabellarisch geführt werden, im Idealfall sind die Anforderungen als S.M.A.R.T.-Ziel formuliert (**s**pezifisch, **m**essbar, **a**ttraktiv, **r**ealisisch, **t**erminiert). Dabei helfen kann z.B. eine Tabelle nach diesem Aufbau:

|Nr. | Name der Anforderung | Beschreibung des Nutzens<br/>(z.B. als Satzschablone) | Akzeptanzkriterium<br/> (wann gilt Anforderung als erfüllt) | Priorisierung / Risiko<br/> bei Nichterfüllung | 
|--- | --- | --- |--- | --- | 
|1. | Einschalten | Das System muss einfach einschaltbar sein, um mit der Benutzung starten zu können. | Einschalten führt innerhalb von 10s in das Startmenü mit aktivierter Anzeige.| _must-have_<br/><br/>Rückweisungskriterium |
|2. | Ausschalten | In jedem Zustand soll sich das System ausschalten lassen, um die Benutzung zu beenden. | Aus allen Zuständen, in denen keine Benutzereingabe erforderlich ist, führt ein Ausschalten nach 20s zum Speichern der aktuellen Daten und herunterfahren.| _must-have_<br/><br/>Systemintegrität gefährdet<br/>möglicher Datenverlust<br/>ggf. Rückweisungskriterium | 
|3. | Stand-by | Im Stand-by-Betrieb soll der Strombedarf minimiert werden, um die Ressourcen zu schonen. | Die gemessene Leistung darf im Stand-By maximal 1W betragen.| _should-have_<br/><br/>Stromkosten für Stand-by |
|4. | Power-off | Das Gerät soll im ausgeschalteten Zustand keinerlei Strom zu verbrauchen, wenn es nicht genutzt wird. | Die Messung der Leistung im "Power-off"-Modus muss 0W betragen.| _should-have_<br/><br/>Schlechte Bewertungen von Nutzern, Abwertung in Tests mit Nachhaltigkeitskriterien, Stromkosten |


#### Beispiel: Gliederung eines komplexeren Lastenhefts für größere Projekte

Bei komplexeren Produkten umfassen Lastenhefte oft mehrere Aktenordner. Die Gliederung ist stark vom jeweiligen Produkttyp abhängig.


1. Zielbestimmung: 

   Welchen Nutzen soll das System erbringen? 

2. Produkteinsatz: 

    Was ist der konkrete Anwendungsfall und das Hauptszenario eines Einsatzes?

    Wer sind die Hauptbenutzer(-gruppen) des Projekts,  (ggf. in Abgrenzung: wer nicht)?

3. Produktumgebung: 

    In welchem Kontext soll das Produkt eingesetzt werden? 

    Was sind die spezifischen Systemgrenzen, welche Rollen gibt es in der Benutzung des Produkts? Mit welchen anderen Systemen interagiert das Produkt?

    Welche Stakeholder (Beteiligte) sind bereits identifiziert, die einbezogen werden sollten?

    Welche Benutzergruppen gibt es?

4. Produktfunktionen mit Akzeptanzkriterien und Priorisierung

    Hier werden die wesentlichen Anforderungen beschrieben - beispielsweise über die tabellarische Darstellung oben. Der Detaillierungsgrad ist abhängig von der Projektumgebung, -sicherheit und -umfang.

5. Qualitätsanforderungen mit Akzeptanzkriterien und Priorisierung

    Bei den Qualitätsanforderungen ist die Beschreibung der Akzeptanzkriterien besonders schwierig, aber auch besonders wichtig. Es empfiehlt sich, die nicht-funktionalen Anforderungen wie Bedienbarkeit, Zuverlässigkeit, Effizienz, Wartbarkeit, Nachhaltigkeit auf messbare Größen zurückzuführen (Beispiel bei Effizienz: Festlegung der Antwortzeit des Systems in Millisekunden).

6. Produktdaten
 
     Gibt es Daten, die über die Programmlaufzeit hinaus gespeichert werden sollen?

     Über welche Zeitdauer müssen die Daten vorgehalten werden? Zu welchem Zweck müssen die Daten erhoben / gespeichert werden?

     Welche schützenswerten Daten werden verarbeitet und welche IT-Sicherheits- und Datenschutz-Maßnahmen sind somit nötig? 

7. Benutzeroberfläche / Bedienelemente

     Welche Festlegungen an die Bedienelemente (haptisch: Taster, Drehgeber, LED, Displays) und Benutzeroberfläche gibt es, die nicht in den Produktfunktionen oder Qualitätsanforderungen beschrieben wurden? Hier können auch Skizzen und Beispiele genannt werden.
 
8. Anwendungsfallbeschreibungen und Hauptszenarien

   Um das Produktverständnis zu verbessern kann es hilfreich sein, eine Übersicht der Anwendungsfälle im Lastenheft zu vermerken (z.B. mit [UML Anwendungsfalldiagramm](https://oer-informatik.de/uml-usecase) und die einzelnen Anwendungsfälle tabellarisch zu beschreiben ([Beispiele siehe hier](https://oer-informatik.de/anforderungen-mit-anwendungsfaellen-beschreiben)). Die wesentlichen Szenarien (also Benutzungswege des Produkts) lassen sich als Stichpunkt-Liste oder Flussdiagramm (Ereignisgesteuerte Prozesskette, [UML-Aktivitätsdiagramm](https://oer-informatik.de/uml-aktivitaetsdiagramm)) dokumentieren.

10. Glossar / Abkürzungsverzeichnis

    Wichtig ist, dass alle Projektteilnehmer über eine gemeinsame und eindeutige Sprache verfügen. Es kann daher hilfreich sein, wenn der Kunde im Lastenheft die Begriffe der Problemdomäne definiert und ggf. für die nicht fachkundigen Entwickler*innen erklärt. Diese Begriffe können dann bei Realisierung in die Lösungsdomäne übernommen werden und Missverständnisse so vermieden werden.

11. Lebenszyklus

    Für welche Lebensdauer ist das Produkt ausgelegt, welche Festlegungen sollen am Ende der Lebenszeit gelten? Muss ein Modularer Aufbau die teilweise Weiternutzung ermöglichen? Welche Ansprüche hinsichtlich der Änderbarkeit des Produkts gibt es am Ende der Lebenszeit? Hier können Vereinbarungen zur Übergabe des Produkts nach der Lebensdauer getroffen werden. Bei wenigen Festlegungen können diese auch als Qualitätsanforderung an die Wartbarkeit und Dauerhaftigkeit mit aufgenommen werden.  


#### Beispiele für Lastenhefte

- [InfoMaPa](http://ftp.uni-kl.de/pub/v-modell-xt/Release-1.2/Beispielprojekte/InfoMaPa/Anforderungen-Lastenheft.pdf)

- [WiBe](http://ftp.uni-kl.de/pub/v-modell-xt/Release-1.2/Beispielprojekte/WiBe/Anforderungen-Lastenheft.pdf)
Den Abschluss dieser Phase bildet im strengen Wasserfall-Sinn die Übergabe eines abgestimmten und qualitätsgesicherten Lastenhefts vom Auftraggeber an den Auftragnehmer.

### Ende der Phase: Lastenheft wird abgenommen und übergeben

Wenn am Abschluss dieser Phase die Anforderungen feststehen, der Kunde sich sicher ist, _*was*_ er will und _*wofür*_ er das Produkt nutzen will und alles in Form eines Lastenhefts dokumentiert ist, sollte das Lastenheft zunächst qualitätsgesichert werden. 

Je nach Projektumfang sollten auf Kundenseite unterschiedliche Abteilungen Reviews mit unterschiedlichem Schwerpunkt vornehmen. Sofern es keine Beanstandungen mehr gibt, wird das Lastenheft vom Auftraggeber unterzeichnet und an den Auftragnehmer übergeben. Die Phase "Anforderungsdefinition" ist damit mit dem Artefakt (Dokument) "Lastenheft" beendet.

Als Nächstes folgt die [Anforderungsanalyse durch den Auftragnehmer](https://oer-informatik.de/wasserfall_mcu_projekt_pflichtenheft), die in der Erstellung eines Pflichtenhefts mündet.
