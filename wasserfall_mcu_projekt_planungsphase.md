## Planungsphase für ein Microcontroller-Projekt

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/wasserfall_mcu_projekt_planung</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Zur Realisiserung kleiner Microcontroller-Projekte wollen wir uns zunächst an dem einfachen dokumenten-getriebenen Vorgehensmodell orientieren: dem Wasserfall-Modell._


![Die Phasen des Wasserfall-Modells](images/wasserfall.png)


### Planung und Entwurf

In der Planungsphase muss die Realisierung des Projekts vor allem in den drei Dimensionen vorbereitet werden:

![Das magische Dreieck des Projektmanagements mit den drei Ecken Qualität/Umfang, Kosten und Zeit](images/magischesDreieck.png)

- Zeit: Welche Meilensteine sollen bis wann erreicht werden, wie viel Zeit steht insgesamt zur Verfügung? 

- Kosten /Budget: Welches Budget ist für das Projekt freigegeben, welche Mittel sind erforderlich und wofür werden sie ausgegeben?

- Qualität / Umfang: Welcher Umfang des Projekts kann realisiert werden? Welche Anforderungen werden umgesetzt? Ist das Projekt nachhaltig?

Diese drei Dimensionen beeinflussen sich gegenseitig - wird beispielsweise die Qualität erhöht, so hat dies direkt Folgen für die Kosten und die Zeit. Wir können nicht alle Dimensionen zeitgleich optimieren und müssen Entscheidungen treffen: Bei klassischen Projekten ist der Umfang vorgegeben, die Zeit und die Kosten bleiben flexibel. Dadurch geraten Kosten und Zeit oft aus dem Ruder. Bei agilen Projekten und Festpreisprojekten wird das magische Dreieck auf den Kopf gestellt: die Projektdauer (Zeit) ist vorgegeben (damit häufig ein wesentlicher Teil der Kosten). Es muss dann jedoch allen Seiten klar sein, dass in diesem Fall die Qualität (der Umfang) zum Projektabschluss nicht im Vorhinein festgelegt werden kann.

#### Planung der Qualität - Hardware

In dieser Phase folgt zunächst die Auswahl der Komponenten, die für die Ein- und Ausgabe nötig sind. 

- Welche Größen müssen gemessen und welche Eingaben müssen ermöglicht werden? (Eingabewerte)

    - Welche unterschiedlichen Techniken (physikalische Prinzipien) stehen zur Ermittlung dieser Größen zur Verfügung? Häufig gibt es unterschiedliche Wege, Größen zu messen.
    
    - Welche Technik wird favorisiert? Warum wurden die anderen Techniken nicht genutzt? Wären andere Techniken für Folgeprojekte denkbar, oder sind sie kategorisch ungeeignet?

    - Welche Bauteile setzen dieses Prinzip um? Welcher Hersteller bietet diese Komponenten an?

    - Welche Hardwareschnittstellen nutzen diese Komponenten, um mit dem Microcontroller zu kommunizieren? Wie erweiterbar ist diese Schnittstelle?

- Welche Aktoren müssen geschaltet werden und  welche Werte müssen ausgegeben werden? (Ausgabewerte) 

  - Welche unterschiedlichen Techniken gibt es, um die gewünschten Ausgaben zu erreichen? (Motoren, Relais, Lautsprecher...)
  
  - Welche Darstellungsformen zur Anzeige bieten sich an? Warum wurde sich gegen andere Darstellungsformen entschieden? (LED, mechanische Zeiger, akkustische/haptische Rückmeldung, Displaytechniken, Webdashboards)

  - Welche Komponenten werden zur Anzeige benötigt? Wer bietet sie an?

  - Welche Hardwareschnittstellen nutzen diese Komponenten, um mit dem Microcontroller zu kommunizieren? Wie erweiterbar ist diese Schnittstelle?

Neben der genauen Auswahl der erforderlichen Komponenten in Form einer Stückliste sollte hier bereits die genaue Verdrahtung geplant werden: welcher Sensor und welcher Aktor wird wie mit dem Microcontroller verschaltet? Werden interne Pull-Up-Widerstände genutzt und sollten später aktiviert werden? Es bietet sich an, die Belegung der Pins direkt als Code-Tabelle zu erstellen, um sie später bei der Implementierung nutzen zu können.

```cpp
//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     PIN_UPDATE_ACTIVE   = 4;               // Pullup, HIGH = Update active
const int     DAC_THERMOSTAT      = 25;              // DAC setting V for analog output
const int     PIN_OPERATING_LED   = 23;              // green LED, HIGH-active
```



#### Planung der Qualität - Software

##### Planung über den Programm-Ablaufplan

```cpp
void setup(){
    //konfigurierePins();

    //initialisiereWLANVerbindung();

    //schalteLEDaufDefault();
}
```

```cpp
void loop(){
    //Einlesen

    //Verarbeiten

    //Ausgeben
}
```

#### Planung der Kosten

#### Planung der Zeit/Ressourcen

## Weitere Literatur und Quellen
