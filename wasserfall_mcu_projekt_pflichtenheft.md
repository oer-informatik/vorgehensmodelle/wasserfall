## Pflichtenheft für ein Microcontroller-Projekt

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/wasserfall_mcu_projekt_pflichtenheft</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/111016716139107981</span>

> **tl/dr;** _(ca. 4 min Lesezeit): Liegen die Anforderungen des Kunden für ein Microcontroller-Projekt vor, so müssen diese nach dem Wasserfall-Modell als nächstes analysiert, mit Realisierungsoptionen versehen und als Pflichtenheft dokumentiert werden._


Auf die [Anforderungsdefinition durch den Kunden](https://oer-informatik.de/wasserfall_mcu_projekt_lastenheft) folgt gemäß des dokumentengetriebenen Wasserfallmodells die zweite Phase, in der diese Anforderungen durch die Entwickler*innen analysiert werden. Der Gesamtablauf des Wasserfallmodells könnte etwa so aussehen: 

![Die Phasen des Wasserfall-Modells](images/wasserfall.png)


### Anforderungsanalyse durch Entwickler*innen (_developer requirements_,"Wie und womit?", Pflichtenheft) 

Aus dem Lastenheft erstellen die Auftragnehmer*innen dann das Pflichtenheft: hier wird Punkt für Punkt beschrieben, _wie und womit_ die im Lastenheft beschriebenen Anforderungen umgesetzt werden sollen. 

Welche Sensoren können die geforderten Messwerte liefern? Welche Anzeige und Bedienelemente bieten die im Lastenheft geforderten Möglichkeiten? Über welche Protokolle können die Daten versendet werden? Sowohl hardware- als auch softwareseitig müssen konkrete Umsetzungsideen genannt werden und deren Aufwände abgeschätzt werden. Hierzu ist eine erste Planungsrunde erforderlich (und damit eine teilweise Vorwegnahme der Schritte aus der kommenden Phase).

Hierbei geht es erstmals nicht nur um die Anforderungen, sondern es muss in einer ersten Übersicht bereits ein _Grobentwurf_ entstehen: neben dem Umfang müssen hier auch Zeit und Kosten abgeschätzt werden. Das Pflichtenheft dient häufig als Angebotsbestandteil und wird somit später Vertragsbestandteil. Daher sollte dieser Grobentwurf detailliert genug für einen Vertrag sein und vor allem potenziell kritische Anforderungen möglichst exakt beschreiben. Insbesondere wichtig ist hierbei, zu beschreiben, was _nicht mehr_ Bestandteil des Projekts ist, damit es im Nachgang nicht zu Streitigkeiten kommt.

Das Pflichtenheft übernimmt und referenziert die Einträge des Lastenhefts. Auch der Aufbau des Lastenhefts bleibt erhalten und wird lediglich um die Realisierungsvorschläge (z.B. unten "per Taster"), zur Qualitätssicherung (Spalte Testfälle) und zum Aufwand ergänzt. Ein tabellarrisches Lastenheft könnte also zum Beispiel diesen Aufbau haben:

|Nr. | Name der Anforderung | Beschreibung des Nutzens und der Realisierung<br/>(z.B. als Satzschablone) | Akzeptanzkriterium<br/> (wann gilt Anforderung als erfüllt) | Grundlegende Testfälle<br/> (Dokumentation von Eingabe, Randbedingung, erwartetem Ergebnis an anderer Stelle) | Priorisierung / Risiko<br/> bei Nichterfüllung | Geschätzter Aufwand|
|--- | --- | --- |--- | --- | --- |--- |
|1. | Einschalten | Das System muss einfach per Taster einschaltbar sein, um mit der Benutzung starten zu können. | Betätigen eines Knopfs führt innerhalb von 10s in das Startmenü mit aktivierer Anzeige.| 1. Einschalten wenn aus: an<br/>2. Einschalten wenn an: Meldung<br/>3. Einschalten während Hochfahren: Meldung<br/>4. Einschalten während Runterfahren: Meldung | _must-have_<br/><br/>Ausschlusskriterium |2h|
|2. | Ausschalten | In jedem Zustand soll das System sich per Knopfdruck ausschalten lassen, um die Benutzung zu beenden. | Aus allen Zuständen, in denen keine Benutzereingabe erforderlich ist, führt ein Betätigen des "Aus"-Knopfs nach 20s zum Speichern der aktuellen Daten und herunterfahren.| 1. Ausschalten wenn aus: keine Reaktion<br/>2. Ausschalten wenn an: runterfahren<br/>3. Ausschalten während Hochfahren: Meldung<br/>4. Ausschalten während Runterfahren: Meldung<br/>5. Ausschalten während Eingabemaske: Meldung | _must-have_<br/><br/>Systemintegrität gefährdet<br/>möglicher Datenverlust<br/>ggf. Rückweisungskriterium | 3h |
|3. | Stand-by | Im Stand-by-Betrieb soll der Strombedarf minimiert werden, um die Ressourcen zu schonen. | Die gemessene Leistung darf im Stand-By maximal 1W betragen.| 1. Messung über die Dauer des Sleep-Modus: durchschnitt 1W<br/>2. Zustandsüberprüfung "Sleep" und "DeepSleep": aufwachen möglich? | _should-have_<br/><br/>Stromkosten von 3,5€/a für jedes Watt Stand-by | 6h |
|4. | Power-off | Das Gerät soll per Schalter vollständig vom Netz getrennt werden können, um keinerlei Strom zu verbrauchen, wenn es nicht genutzt wird. | Die Messung der Leistung im "Power-off"-Modus muss 0W betragen.| 1. Schaltplan: komplette Trennung des Netzteils realisiert?<br/>2. Messung der Leistung im "Power-off" | _should-have_<br/><br/>Schlechte Bewertungen von Nutzern, Abwertung in Tests mit Nachhaltigkeitskriterien | 4h|

### Ende der Phase: Pflichtenheft wird abgenommen und übergeben

Am Ende der Phase haben die Entwickler*innen festgelegt,  _*wie*_ und _*womit*_ die Anforderungen umgesetzt werden sollen. Hierzu wurde das Lastenheft aus der vorigen Phase um Realisierungsdetails ergänzt und bereits ein Grobentwurf vorgenommen. 

Auch das Pflichtenheft sollte einem Review unterzogen werden, bevor es dem Kunden übergeben wird. Schließlich wird es später Vertragsbestandteil und sollte daher frei von Unklarheiten sein. Sofern es keine Beanstandungen mehr gibt, wird das Pflichtenheft vom Auftragnehmer unterzeichnet und an den Auftraggeber übergeben. Die Phase "Anforderungsanalyse" ist damit mit dem Artefakt (Dokument) "Pflichtenheft" beendet.

Häufig wird mit dem Pflichtenheft ein Angebot überreicht, in dem bereits Kostenschätzungen und Termine genannt sind. In diesem Fall beginnt die nächste Phase erst nach Vertragsabschluss durch den Auftraggeber.

Als Nächstes folgt die Feinplanung in der Planungsphase, die in der Erstellung von Planungsdokumenten für Qualität, Zeit und Kosten mündet.
