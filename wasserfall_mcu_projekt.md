## Realisierung eines Microcontroller-Projekts nach dem Wasserfall-Modell

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/wasserfall_mcu_projekt</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Für kleine Projekte, deren Ziel und Anforderungen von Beginn an feststehen, eignet sich phasenweises, dokumentengetriebenes Vorgehen, wie es im Wasserfall-Modell beschrieben wird. Für kleine Microcontroller-Projekte ._


![Die Phasen des Wasserfall-Modells](images/wasserfall.png)


* Anforderungsdefinition durch den Kunden (_customer requirements_): Was und wofür?

* Anforderungsanalyse durch Entwickler*innen (_developer requirements_)

* Planung und Entwurf

* Implementierung / Durchführung

* Qualitätssicherung

* Ausrollen und Übergabe


## Weitere Literatur und Quellen
