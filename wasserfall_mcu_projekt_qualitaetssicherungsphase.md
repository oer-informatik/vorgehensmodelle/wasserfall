## Realisierung eines Microcontroller-Projekts nach dem Wasserfall-Modell

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/mcu_projekt</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

> **tl/dr;** _(ca. 5 min Lesezeit): Zur Realisiserung kleiner Microcontroller-Projekte wollen wir uns zunächst an dem einfachen dokumenten-getriebenen Vorgehensmodell orientieren: dem Wasserfall-Modell._


![Die Phasen des Wasserfall-Modells](images/wasserfall.png)


### Anforderungsdefinition durch den Kunden (_customer requirements_): Was und wofür?

#### Kunde: Was brauche ich und was habe ich davon schon?

Bei Kundenprojekten liegt zunächst der Ball im Feld der Auftraggebenen: sie müssen die grundlegenden Anforderungen an ein Produkt beschreiben: Hier sollte die Frage nach dem _was und wofür_ beantwortet werden. 

Häufig muss dafür der bestehende Prozess, der optimiert werden soll, betrachtet werden ("Ist-Analyse"), um die gewünschten Verbesserungen deutlicher vor Augen zu haben. Falls kein bestehender Prozess existiert, kann es helfen, ähnliche Vorgänge oder Beobachtungen bei anderen gezielt zusammenzutragen.

Diese Ist-Analyse wird um die Verbesserungen und Anpassungen ergänzt, um so den Zielzustand zu formulieren ("Soll-Analyse"). Aus diesem Zielzustand müssen nun Stück für Stück Anforderungen festgelegt und formuliert werden.

Diese Formulierungen münden in einem Lastenheft. In diesem werden zwei unterschiedliche Arten von Anforderungen beschrieben:

- Funktionale Anforderungen: Welches Ergebnis oder welchen Nutzen erbringt das System? 

- Qualitätsanforderungen: Welchen weiteren Anforderungen muss das System genügen, während es den Nutzen erbringt? Gängige Kategorien sind: Bedienbarkeit, Zuverlässigkeit, Effizienz, Wartbarkeit, Nachhaltigkeit.

Technische Details und Realisierungswege werden hier also noch nicht genannt: es geht nicht um Wege, sondern um Ziele.

In der Praxis ist dem Auftraggeber zu diesem Zeitpunkt oft noch gar nicht klar, was er genau will. Sofern bereits Kontakt zu (potenziellen) Auftragnehmenden besteht, ist daher wichtig, bereits in dieser Phase möglichst viele Unklarheiten zu beseitigen. Es gibt eine Reihe von Techniken der [Anforderungs-Ermittlung](https://oer-informatik.de/anforderungsanalyse-ermittlung), die hier angewandt werden können: Workshops, gezielte Befragungen (Inteview oder Frageboden), Prototypen, Marktbeobachtung.

Im Lastenheft sollte beispielsweise festgehalten werden, welche Werte ein IoT-Gerät messen und anzeigen soll, über welche Entfernungen es kommunizieren soll, wie lange die Akkulaufzeit mindestens betragen muss (wenn es sich um ein tragbares Device handelt). Hierbei steht die technische Realisierung wie die Auswahl der Komponenten und Protokolle im Hintergrund: es sollte allein der gewünschte Nutzen beschrieben werden, ohne sich bereits auf einzelne Lösungen festzulegen.

Nach Möglichkeit sollten die Anforderungen bereits kategorisiert und priorisiert werden (siehe z.B. [Anforderungs-Validierung und Verwaltung, MoSCoW-Methode](https://oer-informatik.de/anforderungsanalyse-validieren-verwalten#gliederung-nach-realisierungspriorit%C3%A4t)), um optionale von zwingenden Anforderungen zu trennen.

#### Beispiel: einfaches tabellarisches Lastenheft für kleine Projekte

Der Aufwand für die Erstellung eines Lastenhefts sollte im Verhältnis zum Projektumfang stehen. Für sehr kurze Projekte reicht gegebenenfalls bereits eine kurze Stichpunktliste. Für Projekte, die sich über mehrere Wochen / Monate hinziehen sollte systematisch ein Lastenheft erstellt werden, um keine wesentlichen Punkte zu vergessen.

Ein einfaches Lastenheft kann tabellarisch geführt werden, im Idealfall sind die Anforderungen als S.M.A.R.T.-Ziel formuliert (**s**pezifisch, **m**essbar, **a**ttraktiv, **r**ealisisch, **t**erminiert). Dabei helfen kann z.B. eine Tabelle nach diesem Aufbau:

|Nr. | Name der Anforderung | Beschreibung des Nutzens<br/>(z.B. als Satzschablone) | Akzeptanzkriterium<br/> (wann gilt Anforderung als erfüllt) | Priorisierung / Risiko<br/> bei Nichterfüllung | 
|--- | --- | --- |--- | --- | 
|1. | Einschalten | Das System muss einfach einschaltbar sein, um mit der Benutzung starten zu können. | Einschalten führt innerhalb von 10s in das Startmenü mit aktivierter Anzeige.| _must-have_<br/><br/>Rückweisungskriterium |
|2. | Ausschalten | In jedem Zustand soll sich das System ausschalten lassen, um die Benutzung zu beenden. | Aus allen Zuständen, in denen keine Benutzereingabe erforderlich ist, führt ein Ausschalten nach 20s zum Speichern der aktuellen Daten und herunterfahren.| _must-have_<br/><br/>Systemintegrität gefährdet<br/>möglicher Datenverlust<br/>ggf. Rückweisungskriterium | 
|3. | Stand-by | Im Stand-by-Betrieb soll der Strombedarf minimiert werden, um die Ressourcen zu schonen. | Die gemessene Leistung darf im Stand-By maximal 1W betragen.| _should-have_<br/><br/>Stromkosten für Stand-by |
|4. | Power-off | Das Gerät soll im ausgeschalteten Zustand keinerlei Strom zu verbrauchen, wenn es nicht genutzt wird. | Die Messung der Leistung im "Power-off"-Modus muss 0W betragen.| _should-have_<br/><br/>Schlechte Bewertungen von Nutzern, Abwertung in Tests mit Nachhaltigkeitskriterien, Stromkosten |


#### Beispiel: Gliederung eines komplexeren Lastenhefts für größere Projekte

Bei komplexeren ProduktenBeispielhaft wird hier eine Gliederung vorgestellt, die in Anlehnung
http://gsb.download.bva.bund.de/BIT/V-Modell_XT_Bund/V-Modell%20XT%20Bund%20HTML/14794f684e963e8.html#ref14794f684e963e8


1. Zielbestimmung: 

   Welchen Nutzen soll das System erbringen? 

2. Produkteinsatz: 

    Was ist der konkrete Anwendungsfall und das Hauptszenario eines Einsatzes?

    Wer sind die Hauptbenutzer(-gruppen) des Projekts,  (ggf. in Abgrenzung: wer nicht)?

3. Produktumgebung: 

    In welchem Kontext soll das Produkt eingesetzt werden? 

    Was sind die spezifischen Systemgrenzen, welche Rollen gibt es in der Benutzung des Produkts? Mit welchen anderen Systemen interagiert das Produkt?

    Welche Stakeholder (Beteiligte) sind bereits identifiziert, die einbezogen werden sollten?

    Welche Benutzergruppen gibt es?

4. Produktfunktionen mit Akzeptanzkriterien und Priorisierung

    Hier werden die wesentlichen Anforderungen beschrieben - beispielsweise über die tabellarische Darstellung oben. Der Detaillierungsgrad ist abhängig von der Projektumgebung, -sicherheit und -umfang.

5. Qualitätsanforderungen mit Akzeptanzkriterien und Priorisierung

    Bei den Qualitätsanforderungen ist die Beschreibung der Akzeptanzkriterien besonders schwierig.

6. Produktdaten
 
     Gibt es Daten, die über die Programmlaufzeit hinaus gespeichert werden sollen?

7. Benutzeroberfläche / Bedienelemente
 
8. Anwendungsfallbeschreibungen und Hauptszenarien

7. Produktleistungen

9. Glossar / Abkürzungsverzeichnis

9. Lebenszyklus



#### Beispiele für Lastenhefte

- [InfoMaPa](http://ftp.uni-kl.de/pub/v-modell-xt/Release-1.2/Beispielprojekte/InfoMaPa/Anforderungen-Lastenheft.pdf)

- [WiBe](http://ftp.uni-kl.de/pub/v-modell-xt/Release-1.2/Beispielprojekte/WiBe/Anforderungen-Lastenheft.pdf)
Den Abschluss dieser Phase bildet im strengen Wasserfall-Sinn die Übergabe eines abgestimmten und qualitätsgesicherten Lastenhefts vom Auftraggeber an den Auftragnehmer.

### Anforderungsanalyse durch Entwickler*innen (_developer requirements_)

Aus dem Lastenheft erstellen die Auftragnehmer*innen dann das Pflichtenheft: hier wird Punkt für Punkt beschrieben, _wie und womit_ die im Lastenheft beschriebenen Anforderungen umgesetzt werden sollen. 

Welche Sensoren können die geforderten Messwerte liefern? Welche Anzeige und Bedienelemente bieten die im Lastenheft geforderten Möglichkeiten? Über welche Protokolle können die Daten versendet werden? Sowohl hardware- als auch softwareseitig müssen konkrete Umsetzungsideen genannt werden und deren Aufwände abgeschätzt werden. Hierzu ist eine erste Planungsrunde erforderlich (und damit eine teilweise Vorwegnahme der Schritte aus der kommenden Phase).

Hierbei geht es erstmals nicht nur um die Anforderungen, sondern es muss in einer ersten Übersicht bereits ein _Grobentwurf_ entstehen: neben dem Umfang müssen hier auch Zeit und Kosten abgeschätzt werden. Das Pflichtenheft dient häufig als Angebotsbestandteil und wird somit später Vertragsbestandteil. Daher sollte dieser Grobentwurf detailliert genug für einen Vertrag sein und vor allem potenziell kritische Anforderungen möglichst exakt beschreiben. Insbesondere wichtig ist hierbei, zu beschreiben, was _nicht mehr_ Bestandteil des Projekts ist, damit es im Nachgang nicht zu Streitigkeiten kommt.

Auch das Pflichtenheft kann beispielsweise tabellarisch erstellt werden. Als Ausgangsbasis dient das Lastenheft, dass um weitere Informationen zur Realisierung (z.B. unten "per Taster"), zur Qualitätssicherung (Spalte Testfälle) und zum Aufwand ergänzt wird:

|Nr. | Name der Anforderung | Beschreibung des Nutzens und der Realisierung<br/>(z.B. als Satzschablone) | Akzeptanzkriterium<br/> (wann gilt Anforderung als erfüllt) | Grundlegende Testfälle<br/> (Dokumentation von Eingabe, Randbedingung, erwartetem Ergebnis an anderer Stelle) | Priorisierung / Risiko<br/> bei Nichterfüllung | Geschätzter Aufwand|
|--- | --- | --- |--- | --- | --- |--- |
|1. | Einschalten | Das System muss einfach per Taster einschaltbar sein, um mit der Benutzung starten zu können. | Betätigen eines Knopfs führt innerhalb von 10s in das Startmenü mit aktivierer Anzeige.| 1. Einschalten wenn aus: an<br/>2. Einschalten wenn an: Meldung<br/>3. Einschalten während Hochfahren: Meldung<br/>4. Einschalten während Runterfahren: Meldung | _must-have_<br/><br/>Ausschlusskriterium |2h|
|2. | Ausschalten | In jedem Zustand soll das System sich per Knopfdruck ausschalten lassen, um die Benutzung zu beenden. | Aus allen Zuständen, in denen keine Benutzereingabe erforderlich ist, führt ein Betätigen des "Aus"-Knopfs nach 20s zum Speichern der aktuellen Daten und herunterfahren.| 1. Ausschalten wenn aus: keine Reaktion<br/>2. Ausschalten wenn an: runterfahren<br/>3. Ausschalten während Hochfahren: Meldung<br/>4. Ausschalten während Runterfahren: Meldung<br/>5. Ausschalten während Eingabemaske: Meldung | _must-have_<br/><br/>Systemintegrität gefährdet<br/>möglicher Datenverlust<br/>ggf. Rückweisungskriterium | 3h |
|3. | Stand-by | Im Stand-by-Betrieb soll der Strombedarf minimiert werden, um die Ressourcen zu schonen. | Die gemessene Leistung darf im Stand-By maximal 1W betragen.| 1. Messung über die Dauer des Sleep-Modus: durchschnitt 1W<br/>2. Zustandsüberprüfung "Sleep" und "DeepSleep": aufwachen möglich? | _should-have_<br/><br/>Stromkosten von 3,5€/a für jedes Watt Stand-by | 6h |
|4. | Power-off | Das Gerät soll per Schalter vollständig vom Netz getrennt werden können, um keinerlei Strom zu verbrauchen, wenn es nicht genutzt wird. | Die Messung der Leistung im "Power-off"-Modus muss 0W betragen.| 1. Schaltplan: komplette Trennung des Netzteils realisiert?<br/>2. Messung der Leistung im "Power-off" | _should-have_<br/><br/>Schlechte Bewertungen von Nutzern, Abwertung in Tests mit Nachhaltigkeitskriterien | 4h|

Den Abschluss dieser Phase bildet ein abgestimmtes und qualitätsgesichertes Pflichtenheft.

### Planung und Entwurf

In der Planungsphase muss die Realisierung des Projekts vor allem in den drei Dimensionen vorbereitet werden:

![Das magische Dreieck des Projektmanagements mit den drei Ecken Qualität/Umfang, Kosten und Zeit](images/magischesDreieck.png)

- Zeit: Welche Meilensteine sollen bis wann erreicht werden, wie viel Zeit steht insgesamt zur Verfügung? 

- Kosten /Budget: Welches Budget ist für das Projekt freigegeben, welche Mittel sind erforderlich und wofür werden sie ausgegeben?

- Qualität / Umfang: Welcher Umfang des Projekts kann realisiert werden? Welche Anforderungen werden umgesetzt? Ist das Projekt nachhaltig?

Diese drei Dimensionen beeinflussen sich gegenseitig - wird beispielsweise die Qualität erhöht, so hat dies direkt Folgen für die Kosten und die Zeit. Wir können nicht alle Dimensionen zeitgleich optimieren und müssen Entscheidungen treffen: Bei klassischen Projekten ist der Umfang vorgegeben, die Zeit und die Kosten bleiben flexibel. Dadurch geraten Kosten und Zeit oft aus dem Ruder. Bei agilen Projekten und Festpreisprojekten wird das magische Dreieck auf den Kopf gestellt: die Projektdauer (Zeit) ist vorgegeben (damit häufig ein wesentlicher Teil der Kosten). Es muss dann jedoch allen Seiten klar sein, dass in diesem Fall die Qualität (der Umfang) zum Projektabschluss nicht im Vorhinein festgelegt werden kann.

#### Planung der Qualität - Hardware

In dieser Phase folgt zunächst die Auswahl der Komponenten, die für die Ein- und Ausgabe nötig sind. 

- Welche Größen müssen gemessen und welche Eingaben müssen ermöglicht werden? (Eingabewerte)

    - Welche unterschiedlichen Techniken (physikalische Prinzipien) stehen zur Ermittlung dieser Größen zur Verfügung? Häufig gibt es unterschiedliche Wege, Größen zu messen.
    
    - Welche Technik wird favorisiert? Warum wurden die anderen Techniken nicht genutzt? Wären andere Techniken für Folgeprojekte denkbar, oder sind sie kategorisch ungeeignet?

    - Welche Bauteile setzen dieses Prinzip um? Welcher Hersteller bietet diese Komponenten an?

    - Welche Hardwareschnittstellen nutzen diese Komponenten, um mit dem Microcontroller zu kommunizieren? Wie erweiterbar ist diese Schnittstelle?

- Welche Aktoren müssen geschaltet werden und  welche Werte müssen ausgegeben werden? (Ausgabewerte) 

  - Welche unterschiedlichen Techniken gibt es, um die gewünschten Ausgaben zu erreichen? (Motoren, Relais, Lautsprecher...)
  
  - Welche Darstellungsformen zur Anzeige bieten sich an? Warum wurde sich gegen andere Darstellungsformen entschieden? (LED, mechanische Zeiger, akkustische/haptische Rückmeldung, Displaytechniken, Webdashboards)

  - Welche Komponenten werden zur Anzeige benötigt? Wer bietet sie an?

  - Welche Hardwareschnittstellen nutzen diese Komponenten, um mit dem Microcontroller zu kommunizieren? Wie erweiterbar ist diese Schnittstelle?

Neben der genauen Auswahl der erforderlichen Komponenten in Form einer Stückliste sollte hier bereits die genaue Verdrahtung geplant werden: welcher Sensor und welcher Aktor wird wie mit dem Microcontroller verschaltet? Werden interne Pull-Up-Widerstände genutzt und sollten später aktiviert werden? Es bietet sich an, die Belegung der Pins direkt als Code-Tabelle zu erstellen, um sie später bei der Implementierung nutzen zu können.

```cpp
//-------------------------------------------------------------------------------------
// List of Input- and Output-devices and Pins
//-------------------------------------------------------------------------------------
// Datatype | Name of Variable    | Pin No. connected | Name, Behaviour*/
const int     PIN_UPDATE_ACTIVE   = 4;               // Pullup, HIGH = Update active
const int     DAC_THERMOSTAT      = 25;              // DAC setting V for analog output
const int     PIN_OPERATING_LED   = 23;              // green LED, HIGH-active
```



#### Planung der Qualität - Software

##### Planung über den Programm-Ablaufplan

```cpp
void setup(){
    //konfigurierePins();

    //initialisiereWLANVerbindung();

    //schalteLEDaufDefault();
}
```

```cpp
void loop(){
    //Einlesen

    //Verarbeiten

    //Ausgeben
}
```

#### Planung der Kosten

#### Planung der Zeit/Ressourcen



### Implementierung / Durchführung

[Erstellen einer Dokumentation](https://oer-informatik.de/git04-readme-erstellen)

### Qualitätssicherung


### Ausrollen und Übergabe


## Weitere Literatur und Quellen
